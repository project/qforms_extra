<?php

/**
 * @file
 * Qforms extra date element definition.
 */

module_load_include('inc', 'qforms');
module_load_include('inc', 'qforms_extra');

function qforms_extra_fapi_date($element, array $params = array()) {
  $felement = qforms_fapi_text_element($element, $params);
  if (isset($element['size']) && $element['size'] != '0') {
    $felement['#size'] = check_plain($element['size']);
  }
  $felement['#type'] = 'textfield';
  $felement['#element_validate'] = array('qforms_extra_fapi_date_validate');

  // This function check whether library is added before it add the library.
  // Therefore same library will not be added multiple times
  drupal_add_library('system', 'ui.datepicker');

  drupal_add_js('jQuery(document).ready(function() {
		jQuery( \'input[name="' . $element['key'] . '"]\' ).datepicker();
	});', 'inline');
  return $felement;
}

function qforms_extra_fapi_date_validate($element, &$form_state) {
  // Skip validation if field isn't required and if it is empty.
  if ($element['#required'] == FALSE && empty($element['#value'])) {
    return;
  }
  // @Note - DateTime is introduced in PHP 5.3.0.
  $date = DateTime::createFromFormat('m/d/Y', $element['#value']);
  if (!$date) {
    form_error($element, t('You need to enter a valid date.'));
  }
}

function qforms_extra_element_date($element_data) {
  $element_data['element_title'] = t('Date');
  $element = qforms_element_default($element_data);
  qforms_element_add_size($element_data, $element);
  return $element;
}
